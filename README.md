# Recipebook

[Drupal 9 おいしいレシピ集 2](https://amzn.asia/d/au9Rl2g)
のサンプルコード

## Install Development Prerequisites
Install [Lando](https://github.com/lando/lando/releases)

## 注意点
・Docker Desktop と lando でサポートしているバージョンでご利用ください。
・Drupal 9.5の管理テーマが Claro に変更になっています。Puppeteerでの動作ができないため、composer.json で Drupal 9.4 に固定しています。

lando Drupal multisite
drupal/recommended-project

```
$ git clone https://gitlab.com/bassline121/recipebook.git recipebook
$ cd recipebook
$ lando start
$ lando composer require drush/drush
$ lando composer update
$ lando npm i
$ chmod -R 777 web/sites/
$ lando drush sa
```
