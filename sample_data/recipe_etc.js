/**
 * Drupal Umami food magazine
 * 2022/01/03
 * Drupalの英語でインストール後、言語で日本語を追加した後に実行するファイル。
 * lando node recipe_etc.js
 */

import puppeteer from 'puppeteer';
const RECIPEBOOK_IMPORT_HOST = 'recipe.lndo.site'
const RECIPEBOOK_IMPORT_USER_ID = 'admin';
const RECIPEBOOK_IMPORT_PASSWORD = 'admin';
const LANG = 'en';
const CONTENT_TYPE = 'article';

/**
 * メインロジック
 */
(async () => {
  // Puppeteerの起動
  const browser = await puppeteer.launch({
    headless: true, // Headlessモード
    slowMo: 40, // 実行速度指定のミリ秒
    // devtools: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });

  // 空ページを開く
  const page = await browser.newPage();
  // viewport設定
  await page.setViewport({
    width: 1200,
    height: 2400,
  });
  // host変数
  const $host = RECIPEBOOK_IMPORT_HOST;
  // 表示されるのを待つ
  await page.waitForSelector('body');
  await page.waitForTimeout(1000);
  // ログインURL遷移
  await page.goto('http://' + $host + '/' + LANG + '/user/login', {waitUntil: "domcontentloaded"});
  // ユーザーIDを入力
  await page.type('input[name="name"]', RECIPEBOOK_IMPORT_USER_ID);
  // await page.type('#edit-name', USER_ID);
  // パスワードを入力
  await page.type('input[name="pass"]', RECIPEBOOK_IMPORT_PASSWORD);
  // ログインボタンをクリック
  await page.click('input[name="op"]');
  //表示されるのを待つ
  await page.waitForTimeout(1000);
  // 管理画面URL遷移
  await page.goto('http://' + $host + '/' + LANG + '/admin/content', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);

  // Translations for System information
  await page.goto('http://' + $host + '/' + LANG + '/admin/config/system/site-information/translate', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_sitename = await page.$('#edit-translation-config-names-systemsite-name');
  await input_sitename.click({ clickCount: 3 });
  await page.type('input[name="translation[config_names][system.site][name]"]', 'Umami フードマガジン');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Recipe content type number_of_servings
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/types/manage/recipe/fields/node.recipe.field_number_of_servings/translate', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_servings_title = await page.$('#edit-translation-config-names-fieldfieldnoderecipefield-number-of-servings-label');
  await input_servings_title.click({ clickCount: 3 });
  await page.type('input[name="translation[config_names][field.field.node.recipe.field_number_of_servings][label]"]', 'お皿の数');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Recipe content type recipe_instruction
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/types/manage/recipe/fields/node.recipe.field_recipe_instruction/translate', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_instruction_title = await page.$('#edit-translation-config-names-fieldfieldnoderecipefield-recipe-instruction-label');
  await input_instruction_title.click({ clickCount: 3 });
  await page.type('input[name="translation[config_names][field.field.node.recipe.field_recipe_instruction][label]"]', 'レシピの説明');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Recipe content type Recipe category
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/types/manage/recipe/fields/node.recipe.field_recipe_category/translate', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_category_title = await page.$('#edit-translation-config-names-fieldfieldnoderecipefield-recipe-category-label');
  await input_category_title.click({ clickCount: 3 });
  await page.type('input[name="translation[config_names][field.field.node.recipe.field_recipe_category][label]"]', 'レシピカテゴリ');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations of Umami Recipes Banner
  await page.goto('http://' + $host + '/' + LANG + '/block/4/translations?destination=/en/recipes', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(3000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // ブロックの説明
  const input_recipe_banner_explanation = await page.$('#edit-info-0-value');
  await input_recipe_banner_explanation.click({ clickCount: 3 });
  await page.type('input[name="info[0][value]"]', 'Umamiレシピバナー');
  // タイトル
  const input_recipe_banner_title = await page.$('#edit-field-title-0-value');
  await input_recipe_banner_title.click({ clickCount: 3 });
  await page.type('input[name="field_title[0][value]"]', 'ヴィーガン・チョコレート＆ナッツ・ブラウニー');
  // 概要
  const input_recipe_banner_summary = await page.$('#edit-field-summary-0-value');
  await input_recipe_banner_summary.click({ clickCount: 3 });
  // await input_recipe_banner_summary.press('Backspace');
  await input_recipe_banner_summary.type('この贅沢なブラウニーは、中がグー、外がサクサクしていることが大切です。最高の贅沢です。');
  // URL
  const input_recipe_banner_linkuri = await page.$('#edit-field-content-link-0-uri');
  await input_recipe_banner_linkuri.click({ clickCount: 3 });
  await page.type('input[name="field_content_link[0][uri]"]', '/ja/recipes/vegan-chocolate-and-nut-brownies');
  // リンクテキスト
  const input_recipe_banner_linktitle = await page.$('#edit-field-content-link-0-title');
  await input_recipe_banner_linktitle.click({ clickCount: 3 });
  await page.type('input[name="field_content_link[0][title]"]', 'レシピを見る');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Main navigation block
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/block/manage/umami_main_menu/translate?destination=/en/node', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_mainnavi_title = await page.$('#edit-translation-config-names-blockblockumami-main-menu-settings-label');
  await input_mainnavi_title.click({ clickCount: 3 });
  await page.type('input[name="translation[config_names][block.block.umami_main_menu][settings][label]"]', 'メイン ナビゲーション');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations of Umami Home Banner
  await page.goto('http://' + $host + '/' + LANG + '/block/3/translations?destination=/en/node', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_homebanner_title = await page.$('#edit-field-title-0-value');
  await input_homebanner_title.click({ clickCount: 3 });
  await page.type('input[name="field_title[0][value]"]', '超簡単ベジタリアンパスタベイク');
  // 概要
  const input_homebanner_summary = await page.$('#edit-field-summary-0-value');
  await input_homebanner_summary.click({ clickCount: 3 });
  await input_homebanner_summary.type('ヘルシーなパスタベイクは、究極のコンフォートフードです。このおいしいベイクは、とても手早く準備でき、家族みんなの理想的なメニューです。');
  // URL
  const input_homebanner_linkuri = await page.$('#edit-field-content-link-0-uri');
  await input_homebanner_linkuri.click({ clickCount: 3 });
  await page.type('input[name="field_content_link[0][uri]"]', '/ja/recipes/super-easy-vegetarian-pasta-bake');
  // リンクテキスト
  const input_homebanner_linktitle = await page.$('#edit-field-content-link-0-title');
  await input_homebanner_linktitle.click({ clickCount: 3 });
  await page.type('input[name="field_content_link[0][title]"]', 'レシピを見る');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Frontpage view
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/views/view/frontpage/translate', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  // 言語を追加
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // 表示設定
  await page.click('#edit-page-1 > summary > span:nth-child(1)');
  // ページディスプレイのオプション
  await page.click('#edit-display-options--3 > summary > span:nth-child(1)');
  // ヘッダー
  await page.click('#edit-header--2 > summary > span:nth-child(1)');
  // (空) テキスト
  await page.click('#edit-area--2 > summary > span:nth-child(1)');
  // エリアのフォーマット済みテキスト
  await page.click('#edit-content--3 > summary > span:nth-child(1)');
  // ソースの入力
  await page.click('#cke_147_label'); //ソース
  let input_views_Content = await page.$('#cke_4_contents > textarea');
  await input_views_Content.evaluate(function() {
    document.querySelector('#cke_4_contents > textarea').value = ''
  })
  await input_views_Content.type('<p class="text-align-center">あらゆるシーン、食材、スキルレベルに対応したレシピを検索</p>');
  // 翻訳保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Recipe Collections view
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/views/view/recipe_collections/translate?destination', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  await page.click('#edit-default > summary > span:nth-child(1)');
  await page.click('#edit-display-options > summary > span:nth-child(1)');
  const input_rcv_title = await page.$('#edit-translation-config-names-viewsviewrecipe-collections-display-default-display-options-title');
  await input_rcv_title.click({ clickCount: 3 });
  await input_rcv_title.type('レシピ集');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations of Umami footer promo
  await page.goto('http://' + $host + '/' + LANG + '/block/2/translations?destination=/en/node', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // ブロックの説明
  const input_footer_promo_explanation = await page.$('#edit-info-0-value');
  await input_footer_promo_explanation.click({ clickCount: 3 });
  await page.type('input[name="info[0][value]"]', 'Umamiフッタープロモーション');
  // タイトル
  const input_footer_promo_title = await page.$('#edit-field-title-0-value');
  await input_footer_promo_title.click({ clickCount: 3 });
  await page.type('input[name="field_title[0][value]"]', 'Umamiフードマガジン');
  // 概要
  const input_footer_promo_summary = await page.$('#edit-field-summary-0-value');
  await input_footer_promo_summary.click({ clickCount: 3 });
  await input_footer_promo_summary.type('スキルやノウハウを雑誌限定記事、レシピ、今すぐ手に入れるべき理由が満載です。');
  // URL
  const input_footer_promo_linkuri = await page.$('#edit-field-content-link-0-uri');
  await input_footer_promo_linkuri.click({ clickCount: 3 });
  await page.type('input[name="field_content_link[0][uri]"]', '/about-umami');
  // リンクテキスト
  const input_footer_promo_linktitle = await page.$('#edit-field-content-link-0-title');
  await input_footer_promo_linktitle.click({ clickCount: 3 });
  await page.type('input[name="field_content_link[0][title]"]', '詳細はこちら');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Tell us what you think block
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/block/manage/umami_footer/translate', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_tellus_title = await page.$('#edit-translation-config-names-blockblockumami-footer-settings-label');
  await input_tellus_title.click({ clickCount: 3 });
  await page.type('input[name="translation[config_names][block.block.umami_footer][settings][label]"]', 'あなたのご意見をお聞かせください');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations for Articles aside block
  await page.goto('http://' + $host + '/' + LANG + '/admin/structure/block/manage/umami_views_block__articles_aside_block_1/translate', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(2) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // タイトル
  const input_article_aside_title = await page.$('#edit-translation-config-names-blockblockumami-views-block-articles-aside-block-1-settings-views-label');
  await input_article_aside_title.click({ clickCount: 3 });
  await page.type('input[name="translation[config_names][block.block.umami_views_block__articles_aside_block_1][settings][views_label]"]', 'その他の特集記事');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  // Translations of Umami Disclaimer
  await page.goto('http://' + $host + '/' + LANG + '/block/1/translations', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  await page.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > ul > li > a');
  await page.waitForTimeout(1000);
  // ブロックの説明
  const input_umami_disclaimer_explanation = await page.$('#edit-info-0-value');
  await input_umami_disclaimer_explanation.click({ clickCount: 3 });
  await page.type('input[name="info[0][value]"]', 'Umamiの免責事項');
  // Disclaimer
  await page.click('#cke_29_label'); //ソース
  let input_umami_disclaimer_summary = await page.$('#cke_1_contents > textarea');
  await input_umami_disclaimer_summary.evaluate(function() {
    document.querySelector('#cke_1_contents > textarea').value = ''
  })
  await input_umami_disclaimer_summary.type('<p><strong>Umamiマガジン &amp; Umami出版</strong>は架空のフードマガジンは、説明のための架空の雑誌および出版社です。</p>');
  // 保存ボタンをクリック
  await page.click('#edit-submit');
  await page.waitForTimeout(1000);

  console.log('インポート完了');
  // ブラウザ閉じる
  await browser.close();
  // }
})();
