/**
 * Drupal Umami food magazine
 * 2022/01/03
 * Drupalの英語でインストール後、言語で日本語を追加した後に実行するファイル。
 * lando node recipe_recipe.js
 */

import puppeteer from 'puppeteer';
import neatcsv from 'neat-csv';
import fs from 'fs';
const RECIPEBOOK_IMPORT_HOST = 'recipe.lndo.site'
const RECIPEBOOK_IMPORT_USER_ID = 'admin';
const RECIPEBOOK_IMPORT_PASSWORD = 'admin';
const CSV = 'recipe_recipe.csv';
const LANG = 'en';
const CONTENT_TYPE = 'recipe';

/**
 * 各レシピurlから日本語翻訳を追加。
 * @param {Browser} browser
 * @param {string} url - 各レシピurl
 * @param {string} data - csvデータ
 * @param {int} count - ループ回数
 */
const create_recipeWithChildPage = async (browser, url, data, count) => {

  // 新しいタブを開く
  const childPage = await browser.newPage();
  // viewport設定
  await childPage.setViewport({
    width: 1200,
    height: 2000,
  });
  // 各タームurlへ遷移
  await childPage.goto(url);
  await childPage.waitForTimeout(1000);
  // 記事編集画面でTranslateボタンを押した後、日本語のAddボタン選択
  await childPage.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > ul > li > a');
  // 表示されるのを待つ
  await childPage.waitForTimeout(1000);
  await childPage.waitForSelector('#edit-submit');
  // 名前
  const input_title = await childPage.$('#edit-title-0-value');
  await input_title.click({clickCount: 3});
  await childPage.type('input[name="title[0][value]"]', `${data[count]['title']}`);
  await childPage.waitForTimeout(1000);
  // 概要
  await childPage.click('#cke_29_label'); //ソース
  let input_summary = await childPage.$('#cke_1_contents > textarea');
  await input_summary.evaluate(function() {
    document.querySelector('#cke_1_contents > textarea').value = ''
  })
  await input_summary.type(`${data[count]['field_summary']}`);

  // ingredients
  const input_recipe_ingredients_0 = await childPage.$('#edit-field-ingredients-0-value');
  if (input_recipe_ingredients_0) {
    await input_recipe_ingredients_0.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[0][value]"]', `${data[count]['field_recipe_ingredients0']}`);
  }
  // ingredients
  const input_recipe_ingredients_1 = await childPage.$('#edit-field-ingredients-1-value');
  if (input_recipe_ingredients_1) {
    await input_recipe_ingredients_1.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[1][value]"]', `${data[count]['field_recipe_ingredients1']}`);
  }
  // ingredients
  const input_recipe_ingredients_2 = await childPage.$('#edit-field-ingredients-2-value');
  if (input_recipe_ingredients_2) {
    await input_recipe_ingredients_2.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[2][value]"]', `${data[count]['field_recipe_ingredients2']}`);
  }
  // ingredients
  const input_recipe_ingredients_3 = await childPage.$('#edit-field-ingredients-3-value');
  if (input_recipe_ingredients_3) {
    await input_recipe_ingredients_3.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[3][value]"]', `${data[count]['field_recipe_ingredients3']}`);
  }
  // ingredients
  const input_recipe_ingredients_4 = await childPage.$('#edit-field-ingredients-4-value');
  if (input_recipe_ingredients_4) {
    await input_recipe_ingredients_4.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[4][value]"]', `${data[count]['field_recipe_ingredients4']}`);
  }
  // ingredients
  const input_recipe_ingredients_5 = await childPage.$('#edit-field-ingredients-5-value');
  if (input_recipe_ingredients_5) {
    await input_recipe_ingredients_5.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[5][value]"]', `${data[count]['field_recipe_ingredients5']}`);
  }
  // ingredients
  const input_recipe_ingredients_6 = await childPage.$('#edit-field-ingredients-6-value');
  if (input_recipe_ingredients_6) {
    await input_recipe_ingredients_6.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[6][value]"]', `${data[count]['field_recipe_ingredients6']}`);
  }
  // ingredients
  const input_recipe_ingredients_7 = await childPage.$('#edit-field-ingredients-7-value');
  if (input_recipe_ingredients_7) {
    await input_recipe_ingredients_7.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[7][value]"]', `${data[count]['field_recipe_ingredients7']}`);
  }
  // ingredients
  const input_recipe_ingredients_8 = await childPage.$('#edit-field-ingredients-8-value');
  if (input_recipe_ingredients_8) {
    await input_recipe_ingredients_8.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[8][value]"]', `${data[count]['field_recipe_ingredients8']}`);
  }
  // ingredients
  const input_recipe_ingredients_9 = await childPage.$('#edit-field-ingredients-9-value');
  if (input_recipe_ingredients_9) {
    await input_recipe_ingredients_9.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[9][value]"]', `${data[count]['field_recipe_ingredients9']}`);
  }
  // ingredients
  const input_recipe_ingredients_10 = await childPage.$('#edit-field-ingredients-10-value');
  if (input_recipe_ingredients_10) {
    await input_recipe_ingredients_10.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[10][value]"]', `${data[count]['field_recipe_ingredients10']}`);
  }
  // ingredients
  const input_recipe_ingredients_11 = await childPage.$('#edit-field-ingredients-11-value');
  if (input_recipe_ingredients_11) {
    await input_recipe_ingredients_11.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[11][value]"]', `${data[count]['field_recipe_ingredients11']}`);
  }
  // ingredients
  const input_recipe_ingredients_12 = await childPage.$('#edit-field-ingredients-12-value');
  if (input_recipe_ingredients_12) {
    await input_recipe_ingredients_12.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[12][value]"]', `${data[count]['field_recipe_ingredients12']}`);
  }
  // ingredients
  const input_recipe_ingredients_13 = await childPage.$('#edit-field-ingredients-13-value');
  if (input_recipe_ingredients_13) {
    await input_recipe_ingredients_13.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[13][value]"]', `${data[count]['field_recipe_ingredients13']}`);
  }
  // ingredients
  const input_recipe_ingredients_14 = await childPage.$('#edit-field-ingredients-14-value');
  if (input_recipe_ingredients_14) {
    await input_recipe_ingredients_14.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[14][value]"]', `${data[count]['field_recipe_ingredients14']}`);
  }
  // ingredients
  const input_recipe_ingredients_15 = await childPage.$('#edit-field-ingredients-15-value');
  if (input_recipe_ingredients_15) {
    await input_recipe_ingredients_15.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[15][value]"]', `${data[count]['field_recipe_ingredients15']}`);
  }
  // ingredients
  const input_recipe_ingredients_16 = await childPage.$('#edit-field-ingredients-16-value');
  if (input_recipe_ingredients_16) {
    await input_recipe_ingredients_16.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[16][value]"]', `${data[count]['field_recipe_ingredients16']}`);
  }
  // ingredients
  const input_recipe_ingredients_17 = await childPage.$('#edit-field-ingredients-17-value');
  if (input_recipe_ingredients_17) {
    await input_recipe_ingredients_17.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[17][value]"]', `${data[count]['field_recipe_ingredients17']}`);
  }
  // ingredients
  const input_recipe_ingredients_18 = await childPage.$('#edit-field-ingredients-18-value');
  if (input_recipe_ingredients_18) {
    await input_recipe_ingredients_18.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[18][value]"]', `${data[count]['field_recipe_ingredients18']}`);
  }
  // ingredients
  const input_recipe_ingredients_19 = await childPage.$('#edit-field-ingredients-19-value');
  if (input_recipe_ingredients_19) {
    await input_recipe_ingredients_19.click({clickCount: 3});
    await childPage.keyboard.press('Delete');
    await childPage.type('input[name="field_ingredients[19][value]"]', `${data[count]['field_recipe_ingredients19']}`);
  }
  // Recipe instruction
  await childPage.click('#cke_57_label'); //ソース
  let input_recipe_instruction = await childPage.$('#cke_2_contents > textarea');
  await input_recipe_instruction.evaluate(function() {
    document.querySelector('#cke_2_contents > textarea').value = ''
  })
  await input_recipe_instruction.type(`${data[count]['field_recipe_instruction']}`);

  // 記事保存
  await childPage.click('#edit-submit');
  // 記事が記事が保存されるのを待つ
  await childPage.waitForSelector('.messages--status');
  // 閉じる
  await childPage.close();
};

/**
 * メインロジック
 */
(async () => {
  // Puppeteer起動
  const browser = await puppeteer.launch({
    headless: true, // Headlessモード
    slowMo: 40, // 実行速度指定のミリ秒
    // devtools: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });

  // 空ページを開く
  const page = await browser.newPage();
  // viewportの設定
  await page.setViewport({
    width: 1200,
    height: 2000,
  });
  // host変数
  const $host = RECIPEBOOK_IMPORT_HOST;
  // 表示されるのを待つ
  await page.waitForSelector('body');
  await page.waitForTimeout(1000);
  await page.goto('http://' + $host + '/' + LANG + '/user/login', {waitUntil: "domcontentloaded"});
  // ユーザーIDを入力
  await page.type('input[name="name"]', RECIPEBOOK_IMPORT_USER_ID);
  // パスワードを入力
  await page.type('input[name="pass"]', RECIPEBOOK_IMPORT_PASSWORD);
  // ログインボタンをクリック
  await page.click('input[name="op"]');
  // 表示されるのを待つ
  await page.waitForTimeout(1000);
  await page.waitForSelector('#edit-submit');
  // 管理画面URL遷移
  await page.goto('http://' + $host + '/' + LANG + '/admin/content', {waitUntil: "domcontentloaded"});
  // コンテンツタイプをpageを選択
  await page.select('#edit-type', CONTENT_TYPE);
  // 言語を英語を選択
  await page.select('#edit-langcode', LANG);
  // フィルターを押す
  await page.click('#edit-submit-content');
  await page.waitForTimeout(1000);
  // タイトルを押してソート
  await page.click('#view-title-table-column > a');
  await page.waitForTimeout(1000);
  // リスト取得
  const results = await page.evaluate(() => Array.from(document.querySelectorAll('#views-form-content-page-1 > table > tbody > tr > td > div > div > ul > li.translate.dropbutton-action.secondary-action > a')).map(a => a.href));
  console.log(results.length);//取得数

  // CSV読み込み
  const result = fs.readFileSync(CSV);
  const data = await neatcsv(result, {
    separator: "\t",
    newline: "\n"
  });
  let count = 0;
  for (const url of results) {
      console.log('------------');
    console.log(`処理URL ${url}`);
    console.log(data[count]['title']);

    await create_recipeWithChildPage(browser, url, data, count)
        .then(() => {
          console.log('成功');
        }).catch((error) => {
          console.log(`失敗 ${url}`);
          console.log(error);
        });
    count++;
    }
  console.log('インポート完了');
  // ブラウザ閉じる
  await browser.close();
})();
