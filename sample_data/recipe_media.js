/**
 * Drupal Umami food magazine
 * 2022/01/03
 * Drupalの英語でインストール後、言語で日本語を追加した後に実行するファイル。
 * lando node recipe_media.js
 */

import puppeteer from 'puppeteer';
import neatcsv from 'neat-csv';
import fs from 'fs';
const RECIPEBOOK_IMPORT_HOST = 'recipe.lndo.site'
const RECIPEBOOK_IMPORT_USER_ID = 'admin';
const RECIPEBOOK_IMPORT_PASSWORD = 'admin';
const CSV = './recipe_media.csv';
const LANG = 'en';
const CONTENT_TYPE = 'image';

/**
 * 各メディアurlから日本語翻訳を追加。
 * @param {Browser} browser
 * @param {string} url - 各メディアurl
 * @param {string} data - csvデータ
 * @param {int} count - ループ回数
 */
const create_recipeWithChildPage = async (browser, url, data, count) => {

  // 新しいタブを開く
  const childPage = await browser.newPage();
  // viewport設定
  await childPage.setViewport({
    width: 1200,
    height: 1000,
  });
  // 各メディアurlへ遷移
  await childPage.goto(url);
  await childPage.waitForTimeout(1000);
  // 記事編集画面でTranslateボタンを押した後、日本語のAddボタン選択
  await childPage.click('#block-seven-content > table > tbody > tr:nth-child(3) > td:nth-child(4) > div > div > ul > li > a');
  // 表示されるのを待つ
  await childPage.waitForSelector('#edit-submit');
  // 名前
  const input_media_name = await childPage.$('#edit-name-0-value');
  await input_media_name.click({clickCount: 3});
  await childPage.type('input[name="name[0][value]"]', `${data[count]['name']}`);
  // 名前
  const input_media_alt = await childPage.$('#edit-field-media-image-0-alt');
  await input_media_alt.click({clickCount: 3});
  await childPage.type('input[name="field_media_image[0][alt]"]', `${data[count]['alt']}`);
  // 記事保存
  await childPage.click('#edit-submit');
  // 記事が記事が保存されるのを待つ
  await childPage.waitForSelector('.messages--status');
  // タブを閉じる
  await childPage.close();
};

/**
 * メインロジック
 */
(async () => {
  // Puppeteerの起動
  const browser = await puppeteer.launch({
    headless: true, // Headlessモード
    slowMo: 40, // 実行速度指定のミリ秒
    // devtools: true,
    args: ['--no-sandbox', '--disable-setuid-sandbox']
  });

  // 空ページを開く
  const page = await browser.newPage();
  // viewport設定
  await page.setViewport({
    width: 1200,
    height: 2000,
  });
  //host変数
  const $host = RECIPEBOOK_IMPORT_HOST;
  //表示されるのを待つ
  await page.waitForSelector('body');
  await page.waitForTimeout(1000);
  // ログインURL遷移
  await page.goto('http://' + $host + '/' + LANG + '/user/login', {waitUntil: "domcontentloaded"});
  // ユーザーIDを入力
  await page.type('input[name="name"]', RECIPEBOOK_IMPORT_USER_ID);
  // await page.type('#edit-name', USER_ID);
  // パスワードを入力
  await page.type('input[name="pass"]', RECIPEBOOK_IMPORT_PASSWORD);
  // ログインボタンをクリック
  await page.click('input[name="op"]');
  //表示されるのを待つ
  await page.waitForTimeout(1000);
  // await page.waitForSelector('#edit-submit');
  // タクソノミーのTagsを選択
  await page.goto('http://' + $host + '/' + LANG + '/admin/content/media', {waitUntil: "domcontentloaded"});
  await page.waitForTimeout(1000);
  // コンテンツタイプをpageを選択
  await page.select('#edit-type', CONTENT_TYPE);
  // 言語を英語を選択
  await page.select('#edit-langcode', LANG);
  // フィルターを押す
  await page.click('#edit-submit-media');
  await page.waitForTimeout(1000);
  // メディア名でソート
  await page.click('#view-name-table-column > a');
  await page.waitForTimeout(1000);
  // リスト取得
  const results = await page.evaluate(() => Array.from(document.querySelectorAll('#views-form-media-media-page-list > table > tbody > tr > td.views-field.views-field-operations > div > div > ul > li.translate.dropbutton-action.secondary-action > a')).map(a => a.href));
  console.log(results.length);//取得数

  // CSV読み込み
  const result = fs.readFileSync(CSV);
  const data = await neatcsv(result, {
    separator: "\t",
    newline: "\n"
  });
  let count = 0;
  for (const url of results) {
      console.log('------------');
    console.log(`処理URL ${url}`);
    console.log(data[count]['name']);
    console.log(data[count]['alt']);
    await create_recipeWithChildPage(browser, url, data, count)
        .then(() => {
          console.log('成功');
        }).catch((error) => {
          console.log(`失敗 ${url}`);
          console.log(error);
        });
    count++;
    }
  console.log('インポート完了');
  // ブラウザ閉じる
  await browser.close();
})();
